<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PagesController extends Controller {



    public function main()
    {
        return view('main');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function menu()
    {
        return view('menu.menu');
    }

    public function menuItem($menuSlug)
    {
        switch ($menuSlug) {
            case 'drinks':
                return view('menu.drinks');
                break;
            case 'hot_meals':
                return view('menu.hot-meals');
                break;
            case 'snacks':
                return view('menu.snacks');
                break;
            case 'salads':
                return view('menu.salads');
                break;
            case 'pizza':
                return view('menu.pizza');
                break;
            case 'deserts':
                return view('menu.deserts');
                break;

        }
    }

    public function interview()
    {
        return view('interview');
    }

    public function handleForm(Request $request)
    {

    }





}