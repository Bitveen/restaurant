<?php

$app->get('/', 'PagesController@main');
$app->get('/menu', 'PagesController@menu');
$app->get('/menu/{menuSlug}', 'PagesController@menuItem');



$app->get('/interview', 'PagesController@interview');
$app->get('/contacts', 'PagesController@contacts');
