(function() {
    "use strict";

    // Данные по товарам
    var schema = [
        { id: 1, title: 'Блинчики с клубникой и шоколадом', price: 200 },
        { id: 2, title: 'Торт классический', price: 400 },
        { id: 3, title: 'Мороженое с киви', price: 150 },
        { id: 4, title: 'Мороженое с вишней', price: 100 },
        { id: 5, title: 'Шоколадное суфле с клубникой', price: 220 },
        { id: 6, title: 'Тирамису', price: 300 },
        { id: 7, title: 'Чай с лимоном', price: 100 },
        { id: 8, title: 'Капучино', price: 150 },
        { id: 9, title: 'Латте', price: 150 },
        { id: 10, title: 'Свежевыжатый сок', price: 100 },
        { id: 11, title: 'Чай с мятой', price: 100 },
        { id: 12, title: 'Итальянская паста с соусом', price: 300 },
        { id: 13, title: 'Жареная курица с горчичным соусом', price: 250 },
        { id: 14, title: 'Куриные котлеты с картофлем фри', price: 350 },
        { id: 15, title: 'Форель на гриле с гарниром', price: 400 },
        { id: 16, title: 'Сосиски с гренками', price: 200 },
        { id: 17, title: 'Суп с сухариками', price: 200 },
        { id: 18, title: 'Куриный шашлык', price: 300 },
        { id: 19, title: 'Мясо с овощами', price: 320 },
        { id: 20, title: 'Пицца сырная с овощами', price: 340 },
        { id: 21, title: 'Пицца с салями и грибами', price: 350 },
        { id: 22, title: 'Пицца с овощами и грибами', price: 300 },
        { id: 23, title: 'Пицца с сыром и грибами', price: 400 },
        { id: 24, title: 'Итальянская пицца с овощами', price: 450 },
        { id: 25, title: 'Салат "Овощной"', price: 200 },
        { id: 26, title: 'Салат "Бельгийский"', price: 250 },
        { id: 27, title: 'Салат "Фруктовый"', price: 350 },
        { id: 28, title: 'Салат "Цезарь"', price: 300 }
    ];

    var basketData = document.getElementById('basket-data').innerHTML;
    var basketTemplate = Handlebars.compile(basketData);


    var ShopBasket = (function() {
        // Товары в корзине
        var orders = [];

        var shopButton   = document.querySelector('.shop-button');
        var shopLabel    = document.querySelector('.shop-quantity');
        var basket       = document.querySelector('.basket');
        var closeButton  = basket.querySelector('.basket-close');
        var basketBody   = basket.querySelector('.basket-body');
        var basketFooter = basket.querySelector('.basket-footer');



        closeButton.addEventListener('click', closeBasket, false);
        shopButton.addEventListener('click', showBasket, false);
        basketBody.addEventListener('click', handleBasketClick, false);

        restoreSelectedOrders();



        // Забрать элементы из хранилища при переходе между страницами
        function restoreSelectedOrders() {
            var elements = sessionStorage.getItem('orders');
            if (elements) {
                orders = JSON.parse(elements);
                createRestoredElements();
            }
            updateInfo();
        }



        function createRestoredElements() {
            if (orders.length > 0) {
                var html = "";
                var resultPrice = 0;
                orders.forEach(function(order) {
                    html += basketTemplate({
                        id: order.id,
                        title: order.title,
                        price: order.price,
                        quantity: order.quantity
                    });
                    resultPrice += order.price * order.quantity;
                });
                basketBody.innerHTML = html;
                basketFooter.innerHTML += "<div class='basket-result'>Итого: <span>" + resultPrice + "</span> руб.</div>";
            }
        }


        function showBasket() {
            basket.classList.add('basket-active');
        }

        function closeBasket() {
            basket.classList.remove('basket-active');
        }



        function handleBasketClick(event) {
            event.preventDefault();
            var target = event.target;
            var id;
            var quantity, q;
            // Удалить заказ
            if (target.classList.contains('basket-item-drop')) {
                event.preventDefault();
                id = target.parentNode.parentNode.dataset.id;
                removeOrder(id);
                target.parentNode.parentNode.parentNode.removeChild(target.parentNode.parentNode);
                updateInfo();
            } else if (target.classList.contains('basket-item-plus')) {
                //обновить количество
                id = target.parentNode.parentNode.dataset.id;
                quantity = target.parentNode.querySelector('.basket-item-quantity').firstElementChild;
                q = parseInt(quantity.innerHTML);
                quantity.innerHTML = ++q;
                updateQuantity(id, q);
            } else if (target.classList.contains('basket-item-minus')) {
                //обновить количество
                id = target.parentNode.parentNode.dataset.id;
                quantity = target.parentNode.querySelector('.basket-item-quantity').firstElementChild;
                q = parseInt(quantity.innerHTML);
                if (q == 1) {
                    return;
                }
                quantity.innerHTML = --q;
                updateQuantity(id, q);

            }
        }

        function updateQuantity(id, newQuantity) {
            for (var i = 0; i < orders.length; i++) {
                if (id == orders[i].id) {
                    orders[i].quantity = newQuantity;
                }
            }
            setSessionData();
            updatePrice();
        }


        // Обновить конечную стоимость товаров
        function updatePrice() {
            var result = 0;
            orders.forEach(function(order) {
                result += order.price * order.quantity;
            });
            var basketResult = basketFooter.querySelector('.basket-result');
            if (basketResult) {
                basketResult.querySelector('span').innerHTML = result.toString();
            } else {
                basketFooter.innerHTML += "<div class='basket-result'>Итого: <span>" + result.toString() + "</span> руб.</div>";
            }


        }

        // Обновить содержимое корзины
        function updateInfo() {
            if (orders.length > 0) {
                var checkoutButton = basketFooter.querySelector('.basket-checkout');
                if (!checkoutButton) {
                    checkoutButton = document.createElement('button');
                    checkoutButton.type = "button";
                    checkoutButton.appendChild(document.createTextNode('Оформить заказ'));
                    checkoutButton.className = "basket-checkout";
                    basketFooter.appendChild(checkoutButton);
                }
                shopLabel.innerHTML = orders.length + " заказ(-ов)";
                updatePrice();

            } else {
                basketBody.innerHTML = "<p class='basket-empty'>Заказы отсутствуют.</p>";
                shopLabel.innerHTML = "заказы отсутствуют";
                basketFooter.innerHTML = "";
            }
        }


        // Удалить элемент из корзины
        function removeOrder(id) {
            for (var i = 0; i < orders.length; i++) {
                if (id == orders[i].id) {
                    orders.splice(i, 1);
                    setSessionData();
                    break;
                }
            }
        }



        // Сохранить данные для междустраничного доступа
        function setSessionData() {
            sessionStorage.setItem('orders', JSON.stringify(orders));
        }


        return {
            addOrder: function(order) {
                var elementExists = false;
                for (var i = 0; i < orders.length; i++) {
                    if (order.id === orders[i].id) {
                        elementExists = true;
                        break;
                    }
                }
                if (elementExists) {
                    orders[i].quantity += order.quantity;

                    var elements = basketBody.querySelectorAll('.basket-item');

                    for (i = 0; i < elements.length; i++) {
                        if (elements[i].dataset.id == order.id) {
                            var qElem = elements[i].querySelector('.basket-item-quantity').querySelector('span');
                            var q = parseInt(qElem.innerHTML);
                            qElem.innerHTML = (q + order.quantity);
                            break;
                        }
                    }



                    updatePrice();
                } else {
                    orders.push(order);

                    if (basketBody.querySelector('.basket-empty')) {
                        basketBody.innerHTML = "";
                    }

                    basketBody.innerHTML += basketTemplate(order);
                }






                setSessionData();
                updateInfo();



            }
        };






    })();




    var GoodsArea = {

        init: function() {
            this.goodsArea = document.querySelector('.goods');
            if (this.goodsArea) {
                this.goodsArea.addEventListener('click', this.handleGoodsAreaClick.bind(this) , false);
            }
        },
        handleGoodsAreaClick: function(event) {
            event.preventDefault();
            var self = this;
            var target = event.target;
            var checkButton;
            var footer, label, quantity, container = null, submit, plusButton, minusButton;

            if (target.classList.contains('good-check') && target.tagName.toLowerCase() === 'button') {
                target.classList.add('good-check-active');
                container = target.parentNode.querySelector('.good-additional');
                if (container) {
                    target.classList.remove('good-check-active');
                    container.parentNode.removeChild(container);
                    return;
                }

                checkButton = target;


                footer = target.parentNode;
                label = document.createElement('span');
                quantity = document.createElement('span');
                container = document.createElement('span');
                submit = document.createElement('a');
                plusButton = document.createElement('a');
                minusButton = document.createElement('a');


                plusButton.href = "";
                minusButton.href = "";

                plusButton.classList.add('fa', 'fa-plus', 'good-plus');
                minusButton.classList.add('fa', 'fa-minus', 'good-minus');

                quantity.appendChild(document.createTextNode('1'));
                container.className = "good-additional";

                quantity.className = "good-quantity";


                label.className = "good-quantity-label";
                label.appendChild(document.createTextNode('Количество: '));

                submit.href = "";
                submit.classList.add('fa', 'fa-check', 'good-ok');

                container.appendChild(label);
                container.appendChild(quantity);
                container.appendChild(plusButton);
                container.appendChild(minusButton);
                container.appendChild(submit);

                footer.appendChild(container);



                plusButton.addEventListener('click', this.handleChangeQuantityClick, false);
                minusButton.addEventListener('click', this.handleChangeQuantityClick, false);



                var timer = setTimeout(function() {
                    footer.removeChild(container);
                    target.classList.remove('good-check-active');
                }, 9000);


                submit.addEventListener('click', function(event) {
                    event.preventDefault();
                    var target = event.target;
                    var goodBlock = target.parentNode.parentNode.parentNode.parentNode;
                    var id = parseInt(goodBlock.dataset.id);
                    var q = parseInt(quantity.innerHTML);


                    if (q !== 0) {
                        for (var i = 0; i < schema.length; i++) {
                            if (id === schema[i].id) {
                                break;
                            }
                        }

                        ShopBasket.addOrder({
                            id: schema[i].id,
                            title: schema[i].title,
                            price: schema[i].price,
                            quantity: q
                        });


                        var success = document.createElement('span');
                        success.className = "good-success";
                        success.appendChild(document.createTextNode('Добавлено.'));
                        footer.appendChild(success);
                        setTimeout(function() {
                            footer.removeChild(success);
                        }, 500);

                    }

                    if (timer) {
                        clearTimeout(timer);
                    }

                    checkButton.classList.remove('good-check-active');
                    footer.removeChild(container);






                }, false);

            }

        },
        handleChangeQuantityClick: function(event) {
            event.preventDefault();
            var target = event.target;
            var element = target.parentNode.querySelector('.good-quantity');
            var q = parseInt(element.innerHTML);

            if (target.classList.contains('good-plus')) {
                if (q == 10) {
                    return;
                }
                q++;
            } else if (target.classList.contains('good-minus')) {
                if (q == 0) {
                    return;
                }
                q--;
            }
            element.innerHTML = q;
        }
    };

    GoodsArea.init();


})();