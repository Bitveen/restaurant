var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function() {
    gulp.src('resources/sass/style.scss')
        .pipe(sass({ outputStyle: 'extended' }).on('error', sass.logError))
        .pipe(gulp.dest('public/css'));
});

gulp.task('sass:watch', function() {
    gulp.watch('resources/sass/*.scss', ['sass']);
});