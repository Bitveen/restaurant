@extends('index')

@section('page_title', 'Опрос')

@section('content')
    <h2 class="page-header">Опрос</h2>

    <div class="form">
        <form action="" method="post">
            <div class="form-row">
                <div class="form-col-left">
                    <label for="credentials" class="form-label">Ваша фамилия и имя:</label>
                </div>
                <div class="form-col-right">
                    <input type="text" id="credentials" class="form-input">
                </div>
            </div>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="">Ваш возраст: </label>
                </div>
                <div class="form-col-right">
                    <label class="form-radio-label" for="age-under18"><input name="age" id="age-under18" type="radio"> младше 18</label>
                    <label class="form-radio-label" for="age-18-30"><input name="age" id="age-18-30" type="radio"> 18-30</label>
                    <label class="form-radio-label" for="age-30-45"><input name="age" id="age-30-45" type="radio"> 30-45</label>
                    <label class="form-radio-label" for="age-above45"><input name="age" id="age-above45" type="radio"> старше 45</label>
                </div>
            </div>
            <h3 class="page-header">Оцените наш ресторан</h3>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="kitchen">Кухня: </label>
                </div>
                <div class="form-col-right">
                    <select class="form-select" name="kitchen" id="kitchen">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option selected value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="service">Обслуживание: </label>
                </div>
                <div class="form-col-right">
                    <select class="form-select" name="service" id="service">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option selected value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="">Интерьер: </label>
                </div>
                <div class="form-col-right">
                    <select class="form-select" name="interier" id="">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option selected value="5">5</option>
                    </select>
                </div>

            </div>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="atmosphere">Атмосфера: </label>
                </div>
                <div class="form-col-right">
                    <select class="form-select" name="atmosphere" id="atmosphere">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option selected value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-col-left">
                    <label class="form-label" for="comment">Ваш комментарий: </label>
                </div>
                <div class="form-col-right">
                    <textarea class="form-textarea" name="comment" id="comment"></textarea>
                </div>
            </div>
            <div class="form-footer">
                <button type="submit" class="form-button">Отправить</button>
            </div>
        </form>
    </div>

@endsection