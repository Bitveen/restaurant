@extends('index')

@section('page_title', 'Основное меню - Десерты')

@section('content')
    <h2 class="page-header">Десерты</h2>

    <div class="goods">

        <div class="row">

            <div class="good" data-id="1">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/1.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Блинчики с клубникой и шоколадом</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>200 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="2">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/2.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Торт классический</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>400 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="3">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/3.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Мороженое с киви</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>150 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="good" data-id="4">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/4.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Мороженое с вишней</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>100 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="5">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/5.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Шоколадное суфле с клубникой</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>220 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="6">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/deserts/6.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Тирамису</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>300 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection