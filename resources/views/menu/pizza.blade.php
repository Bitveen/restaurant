@extends('index')

@section('page_title', 'Основное меню - Пицца')

@section('content')
    <h2 class="page-header">Пицца</h2>

    <div class="goods">

        <div class="row">

            <div class="good" data-id="20">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/pizza/1.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Пицца сырная с овощами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>340 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

            <div class="good" data-id="21">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/pizza/2.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Пицца с салями и грибами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>350 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="22">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/pizza/3.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Пицца с овощами и грибами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>300 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="good" data-id="23">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/pizza/4.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Пицца с сыром и грибами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>400 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

            <div class="good" data-id="24">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/pizza/5.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Итальянская пицца с овощами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>450 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>


        </div>


    </div>


@endsection