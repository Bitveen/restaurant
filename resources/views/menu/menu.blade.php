@extends('index')

@section('page_title', 'Меню ресторана')


@section('content')
    <h2 class="page-header">Меню</h2>

    <div class="menu-items">
        <div class="menu-item">
            <div class="menu-item-body">
                <div class="menu-item-img">
                    <a href="/menu/salads"><img src="/images/salads.jpg" alt="Салаты"></a>
                </div>
            </div>
            <div class="menu-item-description">Салаты</div>
        </div>
        <div class="menu-item">
            <div class="menu-item-body">
                <div class="menu-item-img">
                    <a href="/menu/hot_meals"><img src="/images/hot_meals.jpg" alt="Горячие блюда"></a>
                </div>
            </div>
            <div class="menu-item-description">Горячие блюда</div>
        </div>
        <div class="menu-item">
            <div class="menu-item-body">
                <div class="menu-item-img">
                    <a href="/menu/deserts"><img src="/images/deserts.jpg" alt="Десерты"></a>
                </div>
            </div>
            <div class="menu-item-description">Десерты</div>
        </div>
        <div class="menu-item">
            <div class="menu-item-body">
                <div class="menu-item-img">
                    <a href="/menu/pizza"><img src="/images/pizza.jpg" alt="Пицца"></a>
                </div>
            </div>
            <div class="menu-item-description">Пицца</div>
        </div>
        <div class="menu-item">
            <div class="menu-item-body">
                <div class="menu-item-img">
                    <a href="/menu/drinks"><img src="/images/drinks.jpg" alt="Напитки"></a>
                </div>
            </div>
            <div class="menu-item-description">Напитки</div>
        </div>
    </div>
@endsection