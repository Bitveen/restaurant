@extends('index')

@section('page_title', 'Основное меню - Напитки')

@section('content')
    <h2 class="page-header">Напитки</h2>



    <div class="goods">

        <div class="row">

            <div class="good" data-id="7">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/drinks/1.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Чай с лимоном</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>100 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="8">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/drinks/2.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Капучино</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>150 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="9">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/drinks/3.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Латте</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>150 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="good" data-id="10">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/drinks/4.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Свежевыжатый сок</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>100 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="11">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/drinks/5.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Чай с мятой</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>100 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

        </div>


    </div>

@endsection