@extends('index')

@section('page_title', 'Основное меню - Салаты')

@section('content')
    <h2 class="page-header">Салаты</h2>


    <div class="goods">

        <div class="row">
            <div class="good" data-id="25">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/salads/1.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Салат "Овощной"</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>200 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="26">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/salads/2.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Салат "Бельгийский"</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>250 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="27">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/salads/3.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Салат "Фруктовый"</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>350 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="good" data-id="28">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/salads/4.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Салат "Цезарь"</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>300 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>


    </div>




@endsection