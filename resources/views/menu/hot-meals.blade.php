@extends('index')

@section('page_title', 'Основное меню - Горячие блюда')

@section('content')
    <h2 class="page-header">Горячие блюда</h2>

    <div class="goods">

        <div class="row">

            <div class="good" data-id="12">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/1.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Итальянская паста с соусом</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>300 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="13">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/2.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Жареная курица с горчичным соусом</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>250 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="14">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/3.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Куриные котлеты с картофлем фри</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>350 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="good" data-id="15">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/4.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Форель на гриле с гарниром</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>400 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="16">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/5.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Сосиски с гренками</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>200 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="17">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/6.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Суп с сухариками</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>200 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="good" data-id="18">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/7.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Куриный шашлык</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>300 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
            <div class="good" data-id="19">
                <div class="good-body">
                    <div class="good-img">
                        <img src="/images/hot_meals/8.jpg" alt="">
                    </div>
                    <div class="good-description">
                        <span>Мясо с овощами</span>
                    </div>
                    <div class="good-footer">
                        <div class="good-price">Стоимость: <strong>320 руб.</strong></div>
                        <button class="good-check" type="button">Заказать</button>
                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection