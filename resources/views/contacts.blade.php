@extends('index')

@section('page_title', 'Контактная информация')

@section('content')
    <h2 class="page-header">Контакты</h2>
    <div class="row">
        <div class="contacts">
            <h4>Адрес: </h4>
            <p>Невский пр. д. 23</p>
            <h4>Телефоны: </h4>
            <p>343-50-98</p>
            <p>+7(921)-956-93-87</p>
            <h4>Email: </h4>
            <p>rest23@mail.ru</p>
        </div>
        <div class="map"></div>
    </div>



    <script>

        var map;
        function initMap() {
            map = new google.maps.Map(document.querySelector('.map'), {
                center: {lat: 59.9354837, lng: 30.3183174},
                zoom: 15
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrWWVSThVwP3tTlvP_pf7atQunzEQbDrw&callback=initMap" async defer></script>
@endsection