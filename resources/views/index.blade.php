<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>@yield('page_title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<script id="basket-data" type="text/x-handlebars-template">
        <div class="basket-item" data-id="@{{id}}">
            <div class="basket-item-info">
                <span class="basket-item-title">@{{title}}</span>
                <span class="basket-item-price">Цена за шт.: @{{price}} руб.</span>
            </div>
            <div class="basket-item-actions">
                <span class="basket-item-quantity">Количество: <span>@{{quantity}}</span></span>
                <a href="" class="fa fa-plus basket-item-plus"></a>
                <a href="" class="fa fa-minus basket-item-minus"></a>
                <a href="" class="fa fa-close basket-item-drop"></a>
            </div>
        </div>
</script>


<div class="header">
    <div class="container">
        <h3 class="logo">Restaurant</h3>

        <div class="shop-button">
            <span><i class="fa fa-shopping-basket"></i> Корзина: </span>
            <span class="shop-quantity">заказы отсутствуют</span>
        </div>
        <div class="basket">
            <div class="basket-header">
                <span class="basket-header-title">Ваш выбор: </span>
                <button type="button" class="basket-close">Закрыть</button>
            </div>
            <div class="basket-body"></div>
            <div class="basket-footer"></div>
        </div>

        <div class="menu">
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/menu">Меню</a></li>
                <li><a href="/interview">Опрос</a></li>
                <li><a href="/contacts">Контакты</a></li>
            </ul>
        </div>
    </div>
    </div>
    <div class="content">
        <div class="container">
            @yield('content')
        </div>
    </div>
    <div class="footer"></div>


    <script src="/scripts/vendor/handlebars.js"></script>
    <script src="/scripts/basket.js"></script>
</body>
</html>