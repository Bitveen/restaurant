@extends('index')

@section('page_title', 'Главная страница')

@section('content')

    <h2 class="page-header">Добро пожаловать на сайт ресторана</h2>

    <div class="row">
        <div class="main-img">
            <img src="/images/pizza.jpg" alt="">
        </div>
        <div class="main-img">
            <img src="/images/drinks.jpg" alt="">
        </div>
        <div class="main-img">
            <img src="/images/deserts.jpg" alt="">
        </div>
    </div>

    <div class="offers row">
        <div class="offers-item">
            <div class="offers-item-body">
                <div class="offers-item-header">
                    7 преимуществ
                </div>
                <div class="offers-item-content">
                    <ul>
                        <li>Молодоженам - торт или каравай в подарок!</li>
                        <li>Юбилярам - каждому гостю шампанское!</li>
                        <li>Алкогольные напитки, фрукты - Ваши</li>
                        <li>Нет арендной платы и пробкового сбора</li>
                        <li>Нет ограниченной по громкости музыки</li>
                        <li>Опыт обслуживания банкетов с 1994г.</li>
                        <li>Гибкая система цен и скидок!</li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="offers-item">
            <div class="offers-item-body">
                <div class="offers-item-header">
                    По желанию заказчика
                </div>
                <div class="offers-item-content">
                    <ul>
                        <li>Ведущие с эксклюзивной программой</li>
                        <li>Оформление флористикой, шарами</li>
                        <li>Фото и видео-съемка</li>
                        <li>Звуковое и световое оборудование</li>
                        <li>Артисты оригинального жанра</li>
                        <li>Живая музыка</li>
                        <li>Караоке</li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="offers-item">
            <div class="offers-item-body">
                <div class="offers-item-header">
                    Акции
                </div>
                <div class="offers-item-content">
                    <ul>
                        <li>Молодоженам торт или каравай и подкова на счастье в подарок!</li>
                        <li>Юбилярам - бокал шампанского в подарок в ресторане</li>
                        <li>Юбилярам - караоке в подарок</li>
                        <li>В день защитника отечества мужчинам комплимент</li>
                        <li>В день Святого Валентина всем гостям комплимент</li>
                        <li>Компании 20 лет - закажите банкет и получите скидку 10%</li>
                        <li>При заказе банкета праздничный торт по супер цене!</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>




    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
@endsection